CXX = g++
CFLAGS = -Wall -O2 -g `Magick++-config --cppflags`
LFLAGS = -lmgl `Magick++-config --libs`

all:	aica


aica:	aica.o
	$(CXX) aica.o $(LFLAGS) -o aica

aica.o:	aica.cpp
	$(CXX) $(CFLAGS) -c aica.cpp

clean:	
	rm -f *~ *.o aica


