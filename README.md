Activator/Inhibitor Cellular Automata simulator in C++


Requires ImageMagick++ library for writing png files and
MathGL for plotting data.

http://www.imagemagick.org/Magick++/
http://mathgl.sourceforge.net/


Instructions:
create a text file with a list of rules, j1 j2 r1 r2 h
on each line. Example:

1.0 -0.1 2 8 0
1.0 -0.1 5 13 0

pass this file as the comand line argument to the
executable. I've included the params3.txt file I used
to generate my results. The program will do everything
else and generate an index.html web page that links
to the individual results from each experiment.

For access to all my results and my writeup, please
open proj2.html in a browser window.
