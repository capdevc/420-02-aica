#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cmath>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <mgl/mgl_zb.h>
#include <ImageMagick/Magick++.h>
using namespace std;
using namespace Magick;

const int RUNS = 5;
const int DIM = 30.0;

double lg(double x)
{
    return x ? log(x) : 0;
}

class Cella {
public:
    Cella(double, double, int, int, int);
    void initialize(int experiment, int run);
    void dorun();
    void calcs();
    void plot();
    void writepng(const string &);
    void writehtml();

private:
    string dirname;
    int cells[DIM][DIM];
    double corr[DIM/2], ent[DIM/2], mi[DIM/2];
    double avgcorr[DIM/2], avgent[DIM/2], avgmi[DIM/2];
    double j1, j2;
    int r1, r2, h, experiment;
    int run;
    vector<vector <string> > files;
};

Cella::Cella(double j1, double j2, int r1, int r2, int h)
    : j1(j1), j2(j2), r1(r1), r2(r2), h(h)
{
    files.clear();
    for (int r = 0; r < DIM/2; ++r)
        avgcorr[r] = avgent[r] = avgmi[r] = 0;
}

void Cella::initialize(int exp, int rn)
{
    run = rn;
    experiment = exp;

    // make the base directory for this experiment
    stringstream dname;
    dname << "exp" << setw(3) << setfill('0') << experiment << "/";
    dirname = dname.str();
    mkdir(dirname.c_str(), 0755);

    // initialize the CA with random data
    for (int i = 0; i < DIM; ++i) {
        for (int j = 0; j < DIM; ++j)
            cells[i][j] = (rand()%100 < 50) ? 1 : -1;
    }
    return;
}


// run the actual experiment
void Cella::dorun()
{
    int cellorder[DIM*DIM];
    vector<string> runfiles;

    // set the dir
    stringstream fname;
    fname << "run" << setw(3) << setfill('0') << run << "/";
    string rundir = dirname + fname.str();
    mkdir(rundir.c_str(), 0755);
    fname.str("");

    // loop through until state stops changing
    bool changed = true;
    int iteration= 0;
    while (changed) {
        changed = false;
        // write out the current CA state to a png file.
        fname << "aica" << setw(3) << setfill('0') << iteration++ << ".png";
        writepng(rundir + fname.str());
        // store filename to write to html file later
        runfiles.push_back(fname.str());
        fname.str("");

        // randomize the order, set up an array with a permutation of the indexes
        for (int i = 0; i < DIM*DIM; ++i) cellorder[i] = i;
        for (int i = 0; i < DIM*DIM-1; ++i) {
            int n = i + (rand() % (DIM*DIM-i));
            int temp = cellorder[i];
            cellorder[i] = cellorder[n];
            cellorder[n] = temp;
        }

        // step through the permuted indexes
        for (int i = 0; i < DIM*DIM; ++i) {
            int x = cellorder[i] / DIM;
            int y = cellorder[i] % DIM;

            int act = cells[x][y];
            int inh = 0;

            // walk around the diamond summing act and inh
            for (int r = 1; r < r1; ++r) {
                for (int k = 0; k < r; ++k) {
                    act += cells[(x+r-k) % DIM][(y+k) % DIM];
                    act += cells[(x-k+DIM) % DIM][(y+r-k) % DIM];
                    act += cells[(x-r+k+DIM) % DIM][(y-k+DIM) % DIM];
                    act += cells[(x+k) % DIM][(y-r+k+DIM) % DIM];
                }
            }
            for (int r = r1; r < r2; ++r) {
                for (int k = 0; k < r; ++k) {
                    inh += cells[(x+r-k) % DIM][(y+k) % DIM];
                    inh += cells[(x-k+DIM) % DIM][(y+r-k) % DIM];
                    inh += cells[(x-r+k+DIM) % DIM][(y-k+DIM) % DIM];
                    inh += cells[(x+k) % DIM][(y-r+k+DIM) % DIM];
                }
            }

            // update the cell and set changed if it has flipped
            int old = cells[x][y];
            cells[x][y] = (h + j1*act + j2*inh) >= 0 ? 1 : - 1;
            changed =  changed || !(old == cells[x][y]);
        }
    }
    files.push_back(runfiles);
    // calculate correlations, spatial entropies and mutual
    // informations for this stable state
    calcs();
    return;
}

void Cella::calcs()
{
    // initialize some variables used in calculations
    double sub = 0.0;
    double entropy = 0.0;
    double pr11[DIM/2], pr00[DIM/2], pr10[DIM/2];
    for (int i = 0; i < DIM/2; ++i) pr11[i] = pr00[i] = pr10[i] = 0;

    // step through each cell and update counters
    for (int x = 0; x < DIM; ++x) {
        for (int y = 0; y < DIM; ++y) {
            for (int r = 0; r < DIM/2; ++r) {
                // this loop walks 1/4 of the diamond at radius r around a given cell
                // this ensures we only get each pairing once.
                for (int k = 0; k < r; ++k) {
                    corr[r] += cells[x][y] * cells[(x+r-k) % DIM][(y+k) % DIM];
                    pr11[r] += ((cells[x][y] + 1) / 2) * ((cells[(x+r-k) % DIM][(y+k) % DIM] + 1)/2);
                    pr00[r] += ((-1*cells[x][y] +1)/2) * ((-1*cells[(x+r-k) % DIM][(y+k) % DIM] + 1) / 2);
                }
                // we need a special case for r = 0
                if (!r) {
                    corr[r] += 1;
                    pr11[r] += (cells[x][y] + 1) / 2;
                    pr00[r] += (-1*cells[x][y] +1) /2;
                }
            }
            sub += cells[x][y];
            entropy += (cells[x][y] + 1) / 2;
        }
    }

    // use the counters to calc corr, ent and mi, add these values to a set of
    // aggregate arrays to get averages over multiple runs
    double pr1 = entropy/(DIM*DIM);
    double hs = -(pr1 * lg(pr1) + (1-pr1) * (lg(1-pr1)));
    for (int r = 0; r < DIM/2; ++r) {
        double c = max(2.0, 4.0*r); // special case for r = 0
        cout << corr[r] << " " << sub << " " << (2.0*corr[r])/(c*DIM*DIM) << " " << pow(sub/(DIM*DIM), 2);
        avgcorr[r] += corr[r] = abs((2.0*corr[r])/(c*DIM*DIM) - pow(sub/(DIM*DIM), 2));
        cout << " " << corr[r] << endl;
        pr11[r] = (2.0/(c*DIM*DIM)) * pr11[r];
        pr00[r] = (2.0/(c*DIM*DIM)) * pr00[r];
        pr10[r] = 1 - pr11[r] - pr00[r];
        if (!r) pr10[0] = 0;
        avgent[r] += ent[r] = -(pr11[r] * lg(pr11[r]) + pr00[r] * lg(pr00[r]) + pr10[r] * lg(pr10[r]));
        avgmi[r] += mi[r] = 2.0 * hs - ent[r];
    }
    return;
}

void Cella::plot()
{
    // initialize X values, run should have total runs done, divide
    // the aggregate arrays by runs to get the actual averages
    // plot them to a file using mathgl library
    double xs[DIM/2];
    for (int i = 0; i < DIM/2; ++i) {
        xs[i] = i;
        avgcorr[i] /= run;
        avgent[i] /= run;
        avgmi[i] /= run;
    }

    mglGraphZB gr(600, 600);
    mglData x;
    x.Set(xs, DIM/2);
    mglData y0;
    y0.Set(avgcorr, DIM/2);
    mglData y1;
    y1.Set(avgent,DIM/2);
    mglData y2;
    y2.Set(avgmi, DIM/2);
    gr.SetRanges(0, DIM/2, 0, 1);
    gr.YRange(y0);
    gr.YRange(y1, true);
    gr.YRange(y2, true);
    gr.SetTicks('x', 2, 1);
    gr.SetTicks('y', 0.3, 2);
    gr.Axis("xy");
    gr.Label('x', "R");
    gr.Plot(x, y0, "r o#");
    gr.Plot(x, y1, "b o#");
    gr.Plot(x, y2, "g o#");
    gr.AddLegend("Corr", "r o#");
    gr.AddLegend("H", "b o#");
    gr.AddLegend("MI", "g o#");
    gr.Legend(1, 0.8);

    stringstream fname;
    fname << dirname << "graph" << setw(3) << setfill('0') << experiment << ".png";
    gr.WritePNG(fname.str().c_str(), "", false);
    return;
}

// write the cell data to a png file
void Cella::writepng(const string &fname)
{

    stringstream sizestring;
    sizestring << DIM << "x" << DIM;
    Image img("300x300", "white");
    img.magick("PNG");

    for (int i = 0; i < 300; ++i) {
        for (int j = 0; j < 300; ++j) {
            if (cells[i*DIM/300][j*DIM/300] == -1)
                img.pixelColor(i, j, Color("black"));
        }
    }
    img.write(fname);

    return;
}

void Cella::writehtml()
{
    stringstream fname;
    fname << dirname << "results" << setw(3) << setfill('0') << experiment << ".html";
    ofstream outfile(fname.str().c_str());

    outfile << "<h2>Experiment j1=" << j1 << " j2=" << j2 << " r1=" << r1 << " r2=" << r2 << " h=" << h << "</h2>\n";
    outfile << "<h3>Plot</h3>\n";
    outfile << "<img src=\"" << "graph" << setw(3) << setfill('0') << experiment \
        << ".png\" height=500 width=800 border=1>\n";
    for (size_t i = 0; i < files.size(); ++i) {
        outfile << "<h3>Run # " << i+1 << "</h3>" << endl;
        for (size_t j = 0; j < files[i].size(); ++j) {
            outfile << "<img src=\"run" << setw(3) << setfill('0') << i+1 << "/" \
                    << files[i][j] << "\" height=100 width=100 border=1>\n";
        }
    }
    outfile.close();
}

int main(int argc, char const *argv[])
{
    if (argc != 2) {
        cout << "Usage: aica <inputfile>\n";
        return 1;
    }

    srand(time(NULL));

    ifstream infile(argv[1]);
    ofstream outfile("index.html");
    double j1, j2;
    int r1, r2, h;
    int k = 1;

    while (infile >> j1 >> j2 >> r1 >> r2 >> h) {
        Cella ca(j1, j2, r1, r2, h);
        for (int i = 1; i <= RUNS; ++i) {
            ca.initialize(k,i);
            ca.dorun();
        }
        ca.plot();
        ca.writehtml();
        outfile << "<p> <a href=\"exp" << setw(3) << setfill('0') << k << "/results" << setw(3) \
            << setfill('0') << k << ".html\"> Experiment " << k << "</a> : j1= " << j1 << " j2=" << j2 \
            << " r1=" << r1 << " r2=" << r2 << " h=" << h << "</p>\n";
        ++k;
    }
    outfile.close();
    return 0;
}
